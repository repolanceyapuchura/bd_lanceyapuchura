package org.example;

public class Main_Mongo {

    public static void main(String[] args) {
        try {
            DataBaseOperationMongo data = new DataBaseOperationMongo();
            data.insertData();
            data.displayData();
            data.updateData();
            data.displayData();
            data.deleteData();
            data.displayData();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }

    }
}
