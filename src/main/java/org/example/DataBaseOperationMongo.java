package org.example;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Updates;
import com.mongodb.client.result.DeleteResult;
import com.mongodb.client.result.InsertManyResult;
import com.mongodb.client.result.InsertOneResult;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.mongodb.client.model.Filters.eq;

public class DataBaseOperationMongo {
    private final MongoDatabase database;

    public DataBaseOperationMongo() throws Exception {
        String connectionString = "mongodb://root:example@172.22.0.5:27017/admin";
        MongoClient mongoClient = MongoClients.create(connectionString);
        this.database = mongoClient.getDatabase("apartments");
    }

    public void insertData() throws Exception {
        System.out.println("inserting data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        InsertOneResult result = collection.insertOne(new Document().append("nameEdificio", "Kino" +
                " Der Totem").append("piso", 5));
    }

    public void insertManyData() throws Exception {
        System.out.println("inserting MANY data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        List<Document> documents = Arrays.asList(new Document().append("nameEdificio", "Der " +
                "Eisendrache").append("piso", 10), new Document().append("nameEdificio", "Gorod " +
                "Krovi").append("piso", 7), new Document().append("nameEdificio", "Shadows " +
                "Of Evil").append("piso", 10));
        InsertManyResult result = collection.insertMany(documents);
    }

    public void displayData() throws Exception {
        System.out.println("displaying all data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        List<Document> documents = collection.find().into(new ArrayList<>());
        for (Document edificio : documents) {
            System.out.println(edificio.toJson());
        }
    }

    public void displayOneData() throws Exception {
        System.out.println("displaying one data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        Bson query = eq("nameEdificio", "bookB");
        Document edificio = collection.find(query).first();
        System.out.println(edificio.toJson());
    }

    public void updateData() throws Exception {
        System.out.println("updating data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        collection.updateOne(eq("nameEdificio", "revista1"), Updates.set("piso", 1));
    }

    public void deleteData() throws Exception {
        System.out.println("deleting data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        Bson query = eq("nameEdificio", "revista1");
        DeleteResult result = collection.deleteOne(query);
    }

    public void deleteAllData() throws Exception {
        System.out.println("deleting all data");
        MongoCollection<Document> collection = this.database.getCollection("edificio");
        collection.deleteMany(new Document());
    }

}