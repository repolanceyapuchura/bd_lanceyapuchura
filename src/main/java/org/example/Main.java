package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {

  public static void main(String[] args) {
    String jdbcUrl = "jdbc:mysql://localhost:9090/Condominio";
    String user = "root";
    String password = "root";

    try {
      Connection conexion = DriverManager.getConnection(jdbcUrl, user, password);
      DatabaseOperation data = new DatabaseOperation();
      data.insertDataToEdificio();
      data.displayDataToEdificio();
      data.updateDataToEdificio();
      data.updateDataToEdificio();
      conexion.close();
    } catch (SQLException ex) {
      System.out.println(ex.getMessage());
    } catch (Exception e) {
      throw new RuntimeException(e);
    }
  }
}
