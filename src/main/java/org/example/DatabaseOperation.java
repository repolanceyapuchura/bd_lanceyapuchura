package org.example;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class DatabaseOperation {

  private Statement statement;

  public DatabaseOperation() {
    try {
      System.out.println("Create connection.");
      Class.forName("com.mysql.jdbc.Driver");
      Connection connection = DriverManager.getConnection("jdbc:mysql://localhost:9090/Condominio",
          "root", "root");
      this.statement = connection.createStatement();
    } catch (Exception ex) {
      System.out.println(ex.getMessage());
    }
  }

  public void insertDataToEdificio() throws Exception {
    System.out.println("inserting data");
    this.statement.executeUpdate(
        "INSERT INTO edificio(nombre, direccion) VALUES ('Der Eisendrache','Street Asgart, #935')");
  }

  public void displayDataToEdificio() throws Exception {
    System.out.println("display data");
    ResultSet resultSet = this.statement.executeQuery(
        "select * from edificio");
    //ResultSet resultSet = this.statement.executeQuery(query);
    while (resultSet.next()) {
      System.out.println(
          resultSet.getInt("id_edificio") + " : " + resultSet.getString("nombre") + " - "
              + resultSet.getString("direccion"));
    }
  }

  public void updateDataToEdificio() throws Exception {
    System.out.println("updating data");
    this.statement.executeUpdate("update edificio set direccion = 'en aqui xD' where id_edificio = 6");
  }

  public void deleteDataToEdificio() throws Exception {
    System.out.println("deleting data");
    this.statement.executeUpdate("delete from edificio where id_edificio = 5");
  }
}